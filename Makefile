.DEFAULT_GOAL := help

# The .PHONY directive in a Makefile indicates that a target is not a real file.
# It ensures that the target is always executed, regardless of whether a file with the same name exists.
# In this Makefile, the .PHONY directive is used to declare targets that perform specific tasks and do not generate output files.
# Targets declared as .PHONY will run every time they are invoked, regardless of file existence.
.PHONY: setup test env unset show_env help

## Show available targets and their descriptions.
help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
			helpMessage = match(lastLine, /^## (.*)/); \
			if (helpMessage) { \
				split(lastLine, helpArray, "## "); \
				printf "  \033[36m%-20s\033[0m %s\n", $$1, helpArray[2]; \
			} \
		} \
		{ lastLine = $$0 }' $(MAKEFILE_LIST)

# --------------------------
# Environment Variable steps
# --------------------------

export CHILD_FOLDER_PATH ?= "project/country"

export TF_LOG ?= "off"
export TERRAFORM_VERSION ?= 1.6.4

export TG_ENABLED_PROFILE ?= "true"
export TERRAGRUNT_DEBUG ?= "1"
export TERRAGRUNT_LOG_LEVEL ?= "info"
export TERRAGRUNT_SOURCE_UPDATE ?= "false"
export TERRAGRUNT_VERSION ?= 0.53.6


## Set the environment variables redirecting it to a file like .envrc
env:
	@echo 'export CHILD_FOLDER_PATH=${CHILD_FOLDER_PATH}'

	@echo 'export TF_LOG=${TF_LOG}'
	@echo 'export TERRAGRUNT_VERSION=${TERRAFORM_VERSION}'

	@echo 'export TG_ENABLED_PROFILE=${TG_ENABLED_PROFILE}'
	@echo 'export TERRAGRUNT_DEBUG=${TERRAGRUNT_DEBUG}'
	@echo 'export TERRAGRUNT_LOG_LEVEL=${TERRAGRUNT_LOG_LEVEL}'
	@echo 'export TERRAGRUNT_SOURCE_UPDATE=${TERRAGRUNT_SOURCE_UPDATE}'
	@echo 'export TERRAGRUNT_VERSION=${TERRAGRUNT_VERSION}'

## Unset the environment variables.
unset:
	@echo 'unset CHILD_FOLDER_PATH'

	@echo 'unset TF_LOG'
	@echo 'unset TERRAFORM_VERSION'

	@echo 'unset TG_ENABLED_PROFILE'
	@echo 'unset TERRAGRUNT_DEBUG'
	@echo 'unset TERRAGRUNT_LOG_LEVEL'
	@echo 'unset TERRAGRUNT_SOURCE_UPDATE'
	@echo 'unset TERRAGRUNT_VERSION'

## Show the environment variables.
show-env:
	@env

# -------------
# General steps
# -------------

## Setup the repository requirements and check that have all the dependencies installed
setup: setup-python setup-release setup-tf setup-tg

## Perform all the validation checks in the terraform modules
validate: run-pre-commit

# -------------
# Release steps
# -------------

## Setup the release process
setup-release:
	@echo "Setup the release process"
	@echo "-------------------------"
	@echo "NPM version: $(shell npm --version)"
	@echo "NPX version: $(shell npx --version)"
	@echo
	@echo "Install release dependencies"
	@echo "----------------------------"
	@npm install -D semantic-release@v19.0.5 @semantic-release/git@v10.0.1 @semantic-release/changelog@v6.0.3 conventional-changelog-conventionalcommits@v6.1.0
	@echo

## Execute the release process in every module
run-release:
	@echo "Generating release"
	@echo "------------------"
	@npx semantic-release

## Execute the release process in a specific module with the name provided after the release-module. `make release-module-<PATH-TO-MODULE-FROM-REPO-ROOT>`
release-module-%/:
	cd $* && npx semantic-release --no-ci && cd -
	@echo

## Clean release intallation
clean-release:
	@echo "Cleaning release installation"
	@echo "-----------------------------"
	@rm -rf node_modules/
	@echo

# ------------
# Python setup
# ------------

## Setup Terraform dependencies to develop
setup-python:
	@echo "Ensure Python installed"
	@echo "-----------------------"
	@echo "Python version: $(shell python3 --version)"
	@echo "Pipenv version: $(shell pipenv --version)"
	@echo
	@echo "Setup Pipfile"
	@echo "--------------"
	@pipenv install --dev
	@echo
	@echo "Pre-commit version:"
	@echo "-------------------"
	@pipenv run pre-commit --version
	@echo
	@echo "Setup the pre-commit"
	@echo "--------------------"
	@pipenv run pre-commit install-hooks
	@pipenv run pre-commit install
	@pipenv run pre-commit install --hook-type commit-msg
	@echo

## Execute all pre-commit tasks
run-pre-commit:
	@echo "Run pre commit tasks"
	@echo "--------------------"
	@pipenv run pre-commit run --all-files
	@echo

## Clean Terraform dependencies
clean-python:
	@echo "Clean Virtual environment"
	@echo "-------------------------"
	pipenv --rm
	@echo

# ---------------
# Terraform steps
# ---------------

## Setup Terraform dependencies to develop
setup-tf:
	@echo "Set Terraform Version to use"
	@echo "----------------------------"
	@echo $(TERRAFORM_VERSION) > .terraform-version
	@echo
	@echo "Setup Terraform"
	@echo "---------------"
	@tfenv --version
	@echo


# ----------------
# Terragrunt steps
# ----------------

## Setup Terragrunt dependencies to develop
setup-tg:
	@echo "Set Terragrunt Version to use"
	@echo "-----------------------------"
	@echo $(TERRAGRUNT_VERSION) > .terragrunt-version
	@echo
	@echo "Setup Terragrunt"
	@echo "----------------"
	@tgenv --version
	@echo

## Plan all terraform modules
plan-all:
	@cd $(CHILD_FOLDER_PATH) && terragrunt run-all plan && cd -

## Clean Terragrunt folders
clean-tg:
	@rm -rf "**/.terragrunt-cache"
	@echo
