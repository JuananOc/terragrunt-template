# terragrunt-template

Terragrunt template is a project to serve as a template of a Terragrunt project and how to use in across several repositories.

## Usage

```shell
make env > .envrc
make setup
make help
```

## Tool responsibilities

[[Terragrunt]] and [[Terraform]] are used to have different responsibilities:

- Terragrunt configuration: Defines **WHERE** the resources will be deployed and **WHICH** dependencies the resources have.
	- Input variables required for the Terraform module specific to the execution context.
	- Create at runtime the files required for Terragrunt to run in a specific context.
		- `provider.tf`  (generated automatically)
		- `backend.tf` (management of the s3 bucket and the path configured)
- Terraform modules: Define **WHAT** will be deployed:
	- Resources that share the same lifecycle.
	- Have inputs to know context-specific variables.

## Elements

### Reference location units

The units are identifies an infrastructure resource univocally.
Those reference location units are used to create the **folder structure** of the Terragrunt repository.

The kind of information needed for locating a resource is:
- **Context:** Where is located the resource.
- **Type**: What kind of resource is.
- **Identity**: The current resource.

Also depending on your use case can be more units like:
- **Application:** Is that resource part of a bigger solution?
- **Business Domain:** What is the business domain of the resource?

For AWS:

| RLU      | Name                                              | Description                                                                                          | Example                                 |
| -------- | ------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | --------------------------------------- |
| Context  | Project                                           | The project name                                                                                     | Project name                            |
| Context  | Country                                           | Country where resource is located                                                                    | es, fr, aa                              |
| Context  | Account                                           | AWS Account where resource is located                                                                |                                         |
| Context  | Environment                                       | Environment where resource is located                                                                | development, integration, preproduction |
| Context  | Region                                            | AWS Region where resource is located. If has no region defined use `global`.                         | eu-west-1                               |
| Custom   | Business Domain                                   | Business domain where resource is located                                                            | accounting                              |
| Custom   | [[Applications hold same purpose\|Application]]   | Set of stacks used for the same purpose                                                              | Kafka                                   |
| Type     | [[Stack is a set of modules\|Stack]]              | Set of modules that creates infrastructure. Agnostic of applications/business. Only related to infra | VPC, Kong                               |
| Identity | [[Instance is an stack implementation\|Instance]] | Stack implementation that has specific purpose. It's like a new() in [[OOP]]                         | Monitoring                              |
| Type     | [[Module unit\|Module]]                           | A Terraform Module. Contains TF resources that shares same lifecycle                                 | project-terraform-aws-MODULE            |

### Special files

The repository will have special files (at the same level as the units) to define common variables that helps in the development process.
The target of those files:

- Aim to ease Terraform code agnostic to the environment details where is executed (country, account, region, ...)
- DRY principle to write just code related to the infrastructure.
- Increase consistency.
- Make code that promotes just copy-pasting in another scope.

Find below the special files required for this kind of repository:

| Terms                  | Definition                                                                                                  |
| ---------------------- | ----------------------------------------------------------------------------------------------------------- |
| Version files          | Make usage of tools like `tfenv` and `tgenv` to handle the version to use in the repository                 |
| Scope Variables        | Special files used to create variables for all units defined.                                               |
| Parent Terragrunt File | Special file where global variables and configurations lives. **Imported into every child Terragrunt file** |
| Child Terragrunt File  | Terragrunt file with the Terraform module to execute. **Loads parent terragrunt file**                      |
| External dependencies  | Resources not managed by this repository, because they were already created.                                |

## Requisites to use a Terragrunt repository for configuration

- Create Terraform repositories agnostic of the environment.
  That means:
	- Terraform is used just to know WHAT will be deployed, and have high reusability
	- Terragrunt has all the configurations relative to the context (accounts, environments, ...)

The pros of this are:
- Terragrunt gives us an overview of all the infrastructure and context that Terraform doesn't provide.
- Terragrunt manages the code promotion in different folders by copy-pasting.

## How to implement a Terragrunt configuration repository

### 1. Define folder structure based on [[#Reference location units]]

> If you want the repository represents your infrastructure, avoid contextual units like Applications of Business domains that may change without changes on the infrastructure.
> This kind of links between infrastructure and domains should be DOCUMENTED.

### 2. Define order of the [[#Reference location units]] from left to right

> Suggestion order from previous RLU's in AWS:
> `<PROJECT>/<COUNTRY>/<ACCOUNT>/<ENVIRONMENT>/<REGION>/<STACK>/<INSTANCE>/<MODULE>`
> To ensure that all the context information from general to specs is located at the left and the resources (starting from stack are located to the right

This order may be difficult to define in the following cases:
- **Resources without defined region (like S3)**: use `global` folder to locate them.
- **Stacks belonging to several accounts**: It depends on the type of dependency:
   - **With different lifecycle**: If a stack needs another to work, but is only for configuration.
	 For example, A dependency is the EKS module with the Route53 hosted zone. That the EKS module needs because it wants a route53 record to be associated with him.
   - **With same lifecycle**: If an stack needs RESOURCES located on other accounts to make it work.
	 For example, The dependency between the EKS module and the Route53 records.
	The route53 records will be in the DNS account, but the record itself is not a stack (is just the configuration for the EKS module).
	If the EKS module is deleted we want that the route53 resource to be deleted too.
	The deletion of the route53 record doesn't affect the hosted zone at all.
    > To watch all stack elements (in this case, all the route53 resources) Go to the CLI.
    > The repository represents the resources and their relationships, is not a catalog.
- **`module` folder is located inside the `instance`**: Because of [Terragrunt hcl files per folder limitation, having only one per folder.](https://github.com/gruntwork-io/terragrunt/issues/759). 
  This can be avoided if not having a naming module outside, and have it inside every module.

### 3. Create the repository folder structure

Here you have a reference by default for AWS:

```
.
├── Makefile
├── Pipfile
├── Pipfile.lock
├── README.md
├── .terragrunt-version                            # Define the TF version
├── .terraform-version                             # Define the TG version
└── <PROJECT>
    ├── <COUNTRY>
    │   ├── country.hcl                            # Scope Variable file
    │   └── <ACCOUNT_1>/
    │       ├── account.hcl                        # Scope Variable file
    │       ├── <ENVIRONMENT_1>/
    │       │    ├── environment.hcl               # Scope Variable file
    │       │    └── <REGION>/
    │       │        ├── <STACK>/
    │       │        │   ├── <INSTANCE>/
    │       │        │   │   ├── main/                # Module configuration
    │       │        │   │   │    └── terragrunt.hcl  
    │       │        │   │   └── instance.hcl         # Scope Variable file
    │       │        │   └── stack.hcl                # Scope Variable file
    │       │        └── region.hcl                   # Scope Variable file
    │       └── <ENVIRONMENT_2>/
    │           ├── environment.hcl                # Scope Variable file
    │           └── <REGION>/
    │               ├── <STACK>/
    │               │   ├── <INSTANCE>/
    │               │   │   ├── main/              # Module configuration
    │               │   │   │    └── terragrunt.hcl   
    │               │   │   └── instance.hcl       # Scope Variable file
    │               │   └── stack.hcl              # Scope Variable file
    │               └── region.hcl                 # Scope Variable file
    ├── accounts.hcl                               # Accounts information
    ├── applications.hcl                           # Applications information
    ├── backend.tftpl                              # Backend information
    ├── business-domains.hcl                       # Business domain information
    ├── external-dependencies.hcl                  # External dependencies file
    ├── global.hcl                                 # Scope Variable file
    ├── provider.tftpl                             # Provider informations
    └── terragrunt.hcl                             # parent Terragrunt file
```

### 4. Add [[#Special files]] to the folder structure

About the special files some notes:

#### Version files
Version files are mandatory because allows working with different version of Terraform and Terragrunt in the same repository.

Versions for Terraform and Terragrunt are managed by `tgenv` and `tfenf` (installed in Atlantis). Having the `.terragrunt-version` and `.terraform-version` files to define the versions will make that `terragrunt`  and `terraform`  commands executed inside the repository download and use the required versions.

#### Scope variable files

| Name            | File                 | Mandatory | Content                                      |
| --------------- | -------------------- | --------- | -------------------------------------------- |
| project         | project.hcl          | **Yes**   | Global variables for the project.            |
| countries       | countries.hcl        | **Yes**   | Global variables related to account.         |
| accounts        | accounts.hcl         | **Yes**   | Global variables related to account.         |
| environments    | enviroments.hcl      | **Yes**   | Global variables related to environments.    |
| applications    | applications.hcl     | **Yes**   | Global variables related to the applications |
| business-domain | business-domains.hcl | **Yes**   | Global variables related to business domains |

Those variables are referenced inside every unit folder file, so we can maintain the structure in the same file

| Name                     | File            | Mandatory | Content                                                |
| ------------------------ | --------------- | --------- | ------------------------------------------------------ |
| country                  | country.hcl     | **Yes**   | Country variables.                                     |
| account                  | account.hcl     | **Yes**   | Account variables.                                     |
| environment              | environment.hcl | **Yes**   | Environment variables.                                 |
| current_security_account | environment.hcl | **Yes**   | The secutiry account linked to the current environment |
| region                   | region.hcl      | **Yes**   | Region variables                                       |
| stack                    | stack.hcl       | **Yes**   | Stack variables                                        |
| instance                 | instance.hcl    | **Yes**   | Instance variables                                     |

#### Parent and Child Terragrunt file

| Name                   | Filepath                   | Mandatory | Content                 | Usage                                                                                                                                                                                                                                                                                                                                                                                                        |
| ---------------------- | -------------------------- | --------- | ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Parent Terragrunt file | `<PROJECT>/terragrunt.hcl` | Yes       | Core work of the module | In child Terragrunt files:<br><br>- Enable scope variables.<br>- Enable external dependencies.<br>- Create sugar syntax variables to improve readability.<br><br>DRY:<br><br>- Select the application to use<br>- Select the account variables to use<br>- Select the business domain variables to use<br>- Generate the provider files.<br>- Generate the backend and the dynamic path for Terraform states |

The parent Terragrunt file does:
- Enable scope variables: makes them available to every execution.
- Create sugar syntax: improving readability inside the child terragrunt files.
- Generate provider files: manage the account configuration for the Terraform modules generating the file.
- Generate backend file: to manage the state of the file.

##### Enable scope variables

- **Q: Where do we load the scope variable files?
    **Scope variables are defined in the parent Terraform file in the `locals` block.
- **Q: How parent Terragrunt file variables are loaded inside the child Terragrunt file?
    **To use add the following lines to the child `terragrunt.hcl`  file:
    ```
	include "root" {
      path           = find_in_parent_folders()
      expose         = true
      merge_strategy = "deep"
    }
    ```

- **Q: How the `include "root"` block works? What is happening under the hoods?
    **Terragrunt uses the following [configuration parsing order](https://terragrunt.gruntwork.io/docs/getting-started/configuration/#configuration-parsing-order), making that non-evaluable references for just the parent Terragrunt file are solved when executing a `terragrunt plan`  inside the child Terragrunt file.
    This works because the `find_in_parent_folders()` function looks for a `terragrunt.hcl`  (by default) in the parent folder and load it.
    All the variables inside the parent Terragrunt file `inputs` block will be merged in the child Terragrunt file context.


- **Q: How to use these variables inside the child Terragrunt file?
    **Access through the `include.root.locals.VAR_NAME` .
    The `include` loads the parent Terragrunt file, and we access his `locals`  block, enabling access to the variables defined in the parent.


- **Q: What is the process to add a new scope variable for all the child Terragrunt files?**
    1. Create a variable in the scope variable file -`foo`- under the `global.hcl`  scope variable file.

    2. Define inside the `locals`  block of the parent Terragrunt file the variable (using the same variable name), referencing the loaded scope variable.

    3. Add the variable inside the `inputs`  block.
       ```
       # parent-terragrunt-file.hcl
       locals {
         global = read_terragrunt_config(find_in_parent_folders("global.hcl"))
         ...
         foo    = local.global.locals.foo
       }
       inputs {
         foo = local.foo
       }
       ```

    4. Use this variable in the stack using the `include.root.locals.foo` directive.


- **Q: What considerations are to generate promotable code, DRY code?**
    1. Copy-pasting the stack folder in the region folder required should create the same stack with the proper naming automatically.

    2. The previous action **must not require any change in the child Terragrunt file**.

    3. If requires a change in a configuration value, means that the value should be in a higher scope. For example, if the variable depends on the environment, it should be defined inside the environment scope file.


- **Q: If there are two variables for two different purposes with the same name, how can we define it at the upper level without conflicts?
    **The structure to define variables that are purpose-specific is creating a map with the stack name and having another map with the key of the purpose, which has inside the variable defined at the environment level.
    For example:
    ```
      #. Environment scope file "environment.hcl"

      locals {
        # General environement varialbes
        # ...

        stack = {
          purpose = {
            variable_depending_on_environment = "custom-value-for-this-environment"
          }
        }
      }
    ```

    ```
    # child Terraform file

      locals = {
        variable_dependending_on_environment = include.root.locals.environment.locals.stack["purpose"]["variable_dependending_on_environment"]
      }

    ```

    With this kind of configuration, when promoting the code to another environment, the child Terragrunt file doesn't change! After creating the map of the stack with the value in the new environment it will work, and this is the desired behavior.
    If there is an environment dependency can be a problem if we use values for another environment.

##### Create sugar syntax variables to improve readability

There are several variables created in the parent Terragrunt file, just to avoid long references in the child.

Examples of those variables are located in the external dependencies variables and the application ones in order to improve readability. This only should be done when several resources will access these variables.

##### Generate the provider files

By default a non-named provider is created with the current account.

If your module requires more than one provider, you can specify it at `instance.hcl`  or and `stack.hcl`  level (having more priority at the instance level as is more specific the `providers`  variable:

```
providers = {
   account_alias_name_1: {
     name: "account_name_1",
     region_name: "eu-west-1"
   },
   account_alias_name_2:
     name: "account_name_2",
     region_name: "eu-west-1"
   }
}
```

Here we need to add the following information:

- **Account alias name**: is the **alias** name inside the provider that should be defined inside the Terraform module in the `versions.tf`  file.
- **Account Name**: This is the name of the account to use: This name should exist inside the `accounts.hcl`  file.
- **Region Name**: The region that you want to have for the provider. Usually, it's the same region of the file, but may be different.

The providers are generated dynamically using `generate` block that creates the file (or files) inside the Terraform module before executing the internal `terraform` command.

> **NOTE**: Is important to use concrete versions in the providers

**FAQ**

- **Q: How the generation of the provider works internally?
    **Read the `generate`  [block documentation](https://terragrunt.gruntwork.io/docs/reference/config-blocks-and-attributes/#generate) first to know how the block works. The code that we generate here is the provider resource, so we do not create any provider inside the Terraform modules.
- **Q: Can I add more than one provider for one Terraform module?
    **Yes, just need to add the information about the alias for the provider, the account name, and the region of the provider inside the `providers`  variable and Terragrunt will create them.
- **Q: When should I use more than one provider inside a Terraform module?
    **This is required when the resources for your module that shares the same lifecycle, can be created in different accounts.
    A common use case is creating in the same stack module the resources and the route53 records.
- **Q: We use the `default_tags` attribute in the provider to ensure that all the resources have common tags?**
    Yes, in the generated block we added the default tags to all the providers. See the `providers.tftpl`  file to see the current documentation

##### Generate the backend and the dynamic path for Terraform states

The parent Terraform file handles with the `generate`  block the creation of the backends.  [[Terraform state management]]


#### Dependencies created inside the same repository from other stack

- **Q: How the dependencies are handled in Terraform usually?
    **Terraform manages the dependencies to external resources is done using the `data` blocks to retrieve something from the state.For example, we can see how we query AWS to get the subnet CIDR block information using the subnet ID, that is used inside the security group**:
    ```
     	variable "subnet_id" {}

     data "aws_subnet" "selected" {
       id = var.subnet_id
     }

     resource "aws_security_group" "subnet" {
       vpc_id = data.aws_subnet.selected.vpc_id

       ingress {
         cidr_blocks = [data.aws_subnet.selected.cidr_block]
         from_port   = 80
         to_port     = 80
         protocol    = "tcp"
       }
     }
    ```

- **Q: How this changes with Terragrunt?**
    With Terragrunt, we can make this dependency explicit with the [`dependency`](https://terragrunt.gruntwork.io/docs/reference/config-blocks-and-attributes/#dependency) and [`dependencies`](https://terragrunt.gruntwork.io/docs/reference/config-blocks-and-attributes/#dependencies) blocks that are Terragrunt specifics.
    Having Terraform modules defined with single lifecycles and responsibilities leads us to have dependencies between them.
    Following the previous approach with Terragrunt can't avoid the usage of every `data`  source used to provide information from AWS in the Terraform module.

    ```
     variable "subnet_cidr_block" {}
     variable "subnet_vpc_id" {}


     resource "aws_security_group" "subnet" {
       vpc_id = var.subnet_vpc_id

       ingress {
         cidr_blocks = [var.subnet_cidr_block]
         from_port   = 80
         to_port     = 80
         protocol    = "tcp"
       }
     }
    ```

    Now, to get the information to the `subnet_cidr_block`  and the `subnet_vpc_id`  we use the `dependency` the block inside the child Terragrunt file.

    ```
    terraform {
      source = "terraform-custom-modules/security-group//.?ref=v0.3.0"
    }

    ...

    dependency "vpc" {
      config_path = "../../vpc"

      mock_outputs_allowed_terraform_commands = ["plan", "validate"]
      mock_outputs = {
        vpc_id                      = "fake-vpc-id"
        private_subnets             = ["fake-private-subnet-1", "fake-private-subnet-2", "fake-private-subnet-3"]
      }
    }


    ...

    inputs {
      subnet_vpc_id     = dependency.vpc.outputs.vpc_id
      subnet_cidr_block = dependency.vpc.outputs.private_subnets
    }
    ```

With the second approach, the module has some pros:
- Doesn't have specific information related to the scope where the module is created (environment, account, ...).
- The plan will always work, easing the testing.
- We can mock the input of the dependencies with dummy values.
- New members of the team have the dependency defined, and also the path where this dependency is created if they want to deep-watch the dependency chain.
- Can perform full environment validations using `terragrunt plan-all` to ensure that all the infrastructure has no changes.

> If we use dependencies from other repository we can't use the dependency block, so we will require to have the value specified in some variable file harcoded.
>
> In the previous example, if the VPC is in another account, in our `terragrunt.hcl` have the harcoded VPC ID and VPC private subnets values and you need to update it if change the VPC in the other repository


#### Dependencies external to the repository

| Name                  | Filepath                              | Mandatory | Content            | Example of variables of this scope                                                                                      |
| --------------------- | ------------------------------------- | --------- | ------------------ | ----------------------------------------------------------------------------------------------------------------------- |
| external dependencies | `<PROJECT>/external-dependencies.hcl` | No        | External variables | - **dependencies:** Values that should be created inside this repository but are created/handled in other code sources. |

- **Q: How manage the dependencies when the code | scripts are located in another repository?
    **If the dependency of a resource is managed in this repository, we can use the previous section to know how to handle the dependencies using a dependency block and mocking the outputs values. What happens when the dependency is created in another repository, which code is not in this repo? should we use in this case the Terraform "data" block?

    For all the previous situations we have the `external-dependencies.hcl` file that will handle all the external depenedencies. This file will have just one variable called `dependencies` with the same repository cardinality allowing us to define the dependencies with the same structure as the repository.

```
# external-dependencies.hcl
locals {
  dependencies = {
    project = {
      country = {
        account = {
          environment = {
            region = {
              vpc = {
                cidr_block = "172.20.136.0/22",
                id         = "vpc-0c6d4196a3a8dcb91",
                name       = "vpc"
              }
            }
          }
        }
      }
    }
  }
}
```

To use a dependency, we need to add the stack following the scopes (helping us to promote the code without changing the Terragrunt stack code, and the compilation will fail if we don't add the dependency in the new environment).
_An example, using the previous code to add the `vpc.cidr_block` inside the child `Terragrunt file      `_

```
# child terragrunt file
...
locals {
  vpc_cidr_block = include.root.locals.dependencies[include.root.locals.project_code][include.root.locals.country_code][include.root.locals.account_name][include.root.locals.environment_name][include.root.locals.region_name].vpc.cidr_block
}
...
```

Usually, the dependencies make reference to the same region, so we have a set of sugar syntax variables like stack_dependencies to ease the reference.
Also, because the next part never uses variables, we can use dot notation instead of quoting the key in brackets.
The following code is the same as the code above:

```
...
locals {
  vpc_cidr_block = include.root.locals.stack_dependencies.vpc.cidr_block
}
...
```


---
References:
- [Terragrunt official documentation](https://terragrunt.gruntwork.io/docs/)
- [Terragrunt built-in functions](https://terragrunt.gruntwork.io/docs/reference/built-in-functions/)
- [Terraform built-in functions](https://developer.hashicorp.com/terraform/language/functions)
