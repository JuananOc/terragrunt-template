# This file have all the information relative to the regions.
locals {
  global = {
    name       = "eu-west-1" # Need to pass a valid region to generate the provider, so we always pass eu-west-1
    short_name = "glb"
    dirname    = "global"
  }

  eu-west-1 = {
    name       = "eu-west-1"
    short_name = "ew1"
    dirname    = "eu-west-1"
  }

  eu-west-2 = {
    name       = "eu-west-2"
    short_name = "ew2"
    dirname    = "eu-west-2"
  }

  eu-west-3 = {
    name       = "eu-west-3"
    short_name = "ew3"
    dirname    = "eu-west-3"
  }

  eu-central-1 = {
    name       = "eu-central-1"
    short_name = "ec1"
    dirname    = "eu-central-1"
  }

  eu-central-2 = {
    name       = "eu-central-2"
    short_name = "ec2"
    dirname    = "eu-central-2"
  }

  eu-central-3 = {
    name       = "eu-central-3"
    short_name = "ec3"
    dirname    = "eu-central-3"
  }
}
