# This file have all the external dependencies, which are resources created outside this repository
locals {
  dependencies = {
    project = {
      country = {
        account = {
          all = {
            global = {
              iam-assumable-role = {
                root-user = {
                  arn = "arn:aws:iam::012345678912:role/ROOT_MANUAL_ROLE"
                }
              }
            }
          }
        }
      }
    }
  }
}
