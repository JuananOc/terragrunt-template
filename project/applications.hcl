# This file have all the information relative to the applications.
# Remember that relationship between stacks belonging to the same application is in documentation.
locals {
  application = {
    name : "application"
    app_code : "app1"
  }
}
