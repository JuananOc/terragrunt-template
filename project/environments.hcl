# This file have all the information relative to every environment.
# The environment.name is the name of the folders, this can't be avoided.
locals {
  all = {
    name                   = "all"
    short_name             = "all"
    grouped_name           = "live"
    security_account_name  = "security-nonlive"
    s3_state_bucket_region = "eu-west-1"
    s3_state_bucket_name   = "project-account-all-global-s3-tfstate"
    state_dynamodb_table   = "project-account-all-global-dynamodb-tfstate"
  }

  development = {
    name                   = "development"
    short_name             = "dev"
    grouped_name           = "nonlive"
    security_account_name  = "security-nonlive"
    s3_state_bucket_region = "eu-west-1"
    s3_state_bucket_name   = "project-account-nonlive-global-s3-tfstate"
    state_dynamodb_table   = "project-account-nonlive-global-dynamodb-tfstate"
  }

  integration = {
    name                   = "integration"
    short_name             = "int"
    grouped_name           = "nonlive"
    security_account_name  = "obk-secops-nonlive"
    s3_state_bucket_region = "eu-west-1"
    s3_state_bucket_name   = "project-account-nonlive-global-s3-tfstate"
    state_dynamodb_table   = "project-account-nonlive-global-dynamodb-tfstate"
  }

  preproduction = {
    name                   = "preproduction"
    short_name             = "pre"
    grouped_name           = "nonlive"
    security_account_name  = "security-nonlive"
    s3_state_bucket_region = "eu-west-1"
    s3_state_bucket_name   = "project-account-nonlive-global-s3-tfstate"
    state_dynamodb_table   = "project-account-nonlive-global-dynamodb-tfstate"
  }

  nonlive = {
    name                   = "nonlive"
    short_name             = "nlv"
    grouped_name           = "nonlive"
    security_account_name  = "security-nonlive"
    s3_state_bucket_region = "eu-west-1"
    s3_state_bucket_name   = "project-account-nonlive-global-s3-tfstate"
    state_dynamodb_table   = "project-account-nonlive-global-dynamodb-tfstate"
  }

  live = {
    name                   = "live"
    short_name             = "liv"
    grouped_name           = "live"
    security_account_name  = "security-live"
    s3_state_bucket_region = "eu-west-1"
    s3_state_bucket_name   = "project-account-nonlive-global-s3-tfstate"
    state_dynamodb_table   = "project-account-nonlive-global-dynamodb-tfstate"
  }
}
