# This file have all the information relative to every account.
# The account.name is the name of the folders, this can't be avoided.
locals {
  account = {
    id : "012345678912"
    name : "account"
    short_name : "acc"
    infrastructure_provider_role = "InfrastructureProviderRole"
  }
}
