# Use closed versions when referencing to stacks
terraform {
  source = "${include.root.locals.stack.locals.source}//.?ref=v1.0.0"
}

# Mandatory block to load the parent terragrunt file
include "root" {
  path           = find_in_parent_folders()
  expose         = true
  merge_strategy = "deep"
}

dependency "naming-and-tags" {
  config_path = "../naming-and-tags"
  mock_outputs = {
    naming_and_tags = {
      resource = {
        urid = "MOCK-frontend"
        tags = { "resource_type" : "MOCK-frontend" }
      }
    }
  }
}

# Block to store all the variables that can change in the module
locals {
  var = "value"
}

# Variables.tf required for the module.
# Remember that all the inputs inside Parent terragrunt file are merged with this variables!
inputs = {
  terraform_variable_input_1 = local.var
  name                       = dependency.naming-and-tags.outputs.naming_and_tags.resource.urid
}
