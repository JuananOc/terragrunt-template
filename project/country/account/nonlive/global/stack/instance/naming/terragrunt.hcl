# Use closed versions when referencing to stacks
terraform {
  source = "${include.root.locals.stack.locals.source-naming}//.?ref=v1.0.0"
}

# Mandatory block to load the parent terragrunt file
include "root" {
  path           = find_in_parent_folders()
  expose         = true
  merge_strategy = "deep"
}

# Block to store all the variables that can change in the module
locals {
  spec = "instance-spec"
}

# Variables.tf required for the module.
# Remember that all the inputs inside Parent terragrunt file are merged with this variables!
inputs = {
  name = local.spec
}
