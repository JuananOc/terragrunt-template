locals {
  stack_name    = "stack"
  source        = "git::https://github.com/terraform-aws-modules/project-terraform-aws-stack.git"
  source-naming = "git::https://github.com/terraform-aws-modules/project-terraform-aws-naming.git"
}
