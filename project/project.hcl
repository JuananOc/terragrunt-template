# This file have all the information relative to the project
locals {
  project_name = "terragrunt-template"
  project_code = "TT"

  # S3 state global properties
  root_state_dir = "infrastructure-tg-deployment"
}
