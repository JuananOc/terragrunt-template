locals {
  # ==========================================
  # Load scope variable from UNITS of the path
  # ==========================================
  project      = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  country     = read_terragrunt_config(find_in_parent_folders("country.hcl"))
  account     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment = read_terragrunt_config(find_in_parent_folders("environment.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  stack       = read_terragrunt_config(find_in_parent_folders("stack.hcl"))
  instance    = read_terragrunt_config(find_in_parent_folders("instance.hcl"))

  # =====================
  # Load global variables
  # =====================
  accounts              = read_terragrunt_config(find_in_parent_folders("accounts.hcl"))
  applications          = read_terragrunt_config(find_in_parent_folders("applications.hcl"))
  countries             = read_terragrunt_config(find_in_parent_folders("countries.hcl"))
  environments          = read_terragrunt_config(find_in_parent_folders("environments.hcl"))
  external_dependencies = read_terragrunt_config(find_in_parent_folders("external-dependencies.hcl"))
  regions               = read_terragrunt_config(find_in_parent_folders("regions.hcl"))

  # ============================
  # Shortcut for scope variables
  # ============================
  # Projects
  # --------
  project_code = local.project.locals.project_code
  project_name = local.project.locals.project_name

  # Country
  # -------
  current_country = local.countries.locals[local.country.locals.country]
  area_code       = local.current_country.area_code
  country_code    = local.current_country.code
  country_name    = local.current_country.name

  # Account
  # -------
  current_account              = local.accounts.locals[local.account.locals.account]
  account_id                   = local.current_account.id
  account_name                 = local.current_account.name
  account_short_name           = local.current_account.short_name
  infrastructure_provider_role = local.current_account.infrastructure_provider_role

  # Environment
  # -----------
  current_environment                = local.environments.locals[local.environment.locals.environment]
  environment_name                   = local.current_environment.name
  environment_short_name             = local.current_environment.short_name
  environment_grouped_name           = local.current_environment.grouped_name
  environment_s3_state_bucket_region = local.current_environment.s3_state_bucket_region
  environment_s3_state_bucket_name   = local.current_environment.s3_state_bucket_name
  environment_state_dymamodb_table   = local.current_environment.state_dynamodb_table

  current_security_account    = local.current_environment.security_account_name
  security_account_id         = local.accounts.locals[local.current_security_account].id
  security_account_name       = local.accounts.locals[local.current_security_account].name
  security_account_short_name = local.accounts.locals[local.current_security_account].short_name

  # Region
  # ------
  current_region    = local.regions.locals[local.region.locals.region]
  region_name       = local.current_region.name
  region_short_name = local.current_region.short_name
  region_dirname    = local.current_region.dirname

  # Stack
  # -----
  stack_name = local.stack.locals.stack_name

  # Instance
  # --------
  instance_name = local.instance.locals.instance_name

  # ===================================
  # Load application specific variables
  # ===================================
  application          = try(local.applications.locals[local.instance.locals.application], "")
  application_name     = local.application.name
  app_code             = local.application.app_code

  # =========================
  # Load additional providers
  # =========================
  # {
  #   account_alias_name_1: {
  #     name: "account_name_1",
  #     region_name: "eu-west-1"
  #   },
  #   account_alias_name_2:
  #     name: "account_name_2",
  #     region_name: "eu-west-1"
  #   }
  # }
  providers = try(local.instance.locals.providers, local.stack.locals.providers, {})

  additional_providers = {
    for alias, account in local.providers :
    alias => {
      "id"                           = local.accounts.locals[account.name].id,
      "name"                         = local.accounts.locals[account.name].name,
      "infrastructure_provider_role" = local.accounts.locals[account.name].infrastructure_provider_role,
      "region_name"                  = account.region_name
    }
  }

  # ============================================
  # Shortcut for external dependencies variables
  # ============================================
  dependencies             = local.external_dependencies.locals.dependencies
  country_dependencies     = try(local.dependencies[local.project_code][local.country_code], {})
  account_dependencies     = try(local.dependencies[local.project_code][local.country_code][local.account_name], {})
  environment_dependencies = try(local.dependencies[local.project_code][local.country_code][local.account_name][local.environment_name], {})
  region_dependencies      = try(local.dependencies[local.project_code][local.country_code][local.account_name][local.environment_name][local.region_dirname], {})

  # ========================================
  # Shortcut for internal dependencies paths
  # ========================================
  country_path     = format("%s/%s", path_relative_from_include(), local.country_code)
  account_path     = format("%s/%s", local.country_path, local.account_name)
  environment_path = format("%s/%s", local.account_path, local.environment_name)
  region_path      = format("%s/%s", local.environment_path, local.region_dirname)
  stack_path       = format("%s/%s", local.region_path, local.stack_name)
  instance_path    = format("%s/%s", local.stack_path, local.instance_name)

  # =====================
  # Development variables
  # =====================
  # Enable usage of profile instead of assume role for develop in local
  enabled_profile = get_env("TG_ENABLED_PROFILE", false)
}

# =====================================================
# Add default inputs for all the terragrunt child files
# =====================================================
inputs = merge(
  local.project.locals,
  local.country.locals,
  local.account.locals,
  local.environment.locals,
  local.region.locals,
  local.stack.locals,
  local.instance.locals,
  local.external_dependencies.locals,

  {
    # Expose country selected variables
    area_code    = local.area_code
    country_code = local.country_code
    country_name = local.country_name

    # Expose account selected variables
    account_name       = local.account_name,
    account_short_name = local.account_short_name,
    account_id         = local.account_id,

    # Expose the current security (secops) account
    security_account_id         = local.security_account_id
    security_account_name       = local.security_account_name
    security_account_short_name = local.security_account_short_name

    # Expose environment selected variables
    environment_name                 = local.environment_name
    environment_short_name           = local.environment_short_name
    environment_grouped_name         = local.environment_grouped_name
    environment_s3_state_bucket_name = local.environment_s3_state_bucket_name
    environment_state_dymamodb_table = local.environment_state_dymamodb_table

    # Expose region selected variables
    region_name       = local.region_name
    region_short_name = local.region_short_name

    # Expose application selected variables
    application_name = local.application_name
    app_name         = local.application_name
    app_code         = local.app_code
  }
)

# ===================
# Provider Generation
# ===================
generate "providers" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = templatefile("providers.tftpl", {
    # Account
    account_id                   = local.account_id
    account_name                 = local.account_name
    infrastructure_provider_role = local.infrastructure_provider_role
    additional_providers         = local.additional_providers

    # Region
    region_name = local.region_name

    # Development
    enabled_profile = local.enabled_profile
  })
}

# ==================
# Backend generation
# ==================
# We will have just one s3 bucket for all the accounts managed by this repository: https://confluence-orangebank.valiantys.net/display/INF/14+-+Define+the+Terraform+s3+buckets+to+be+created+to+store+the+Terraform+state
# The bucket is created in https://bitbucket-orangebank.valiantys.net/projects/PLATISS/repos/cloud-infra-socle/browse/terraform/onebank-tfstate-buckets
# and https://jenkins.prod.fr.aws.bank/job/FR-ISS-EU-WEST-1-PROD-CLOUD-INFRA-SOCLE-ONEBANK-TFSTATE-TERRAFORM-PLAN/
generate "s3_backend" {
  path      = "backend.tf"
  if_exists = "overwrite_terragrunt"
  contents = templatefile("backend.tftpl", {
    s3_bucket_state_name                  = local.environment_s3_state_bucket_name
    dynamodb_table                        = local.environment_state_dymamodb_table
    encrypt                               = true
    key                                   = "${local.project.locals.root_state_dir}/${path_relative_to_include()}/terraform.tfstate"
    region_name                           = local.environment_s3_state_bucket_region
    environment_name                      = local.environment_name
    enabled_profile                       = local.enabled_profile
    s3_bucket_account_id                  = local.accounts.locals.obk-devops.id
    s3_bucket_account_name                = local.accounts.locals.obk-devops.name
    s3_bucket_infrastruture_provider_role = local.accounts.locals.obk-devops.infrastructure_provider_role
  })
}
