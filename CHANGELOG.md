# Changelog

## [2.1.0](https://gitlab.com/JuananOc/terragrunt-template/compare/v2.0.0...v2.1.0) (2023-11-26)


### Features

* updated makefile with more detailed actions ([4a12d5f](https://gitlab.com/JuananOc/terragrunt-template/commit/4a12d5f1871b7e0fa251f1af803c65ae656d46d6))


### Bug Fixes

* added the release process with cleaner steps ([de2aa1f](https://gitlab.com/JuananOc/terragrunt-template/commit/de2aa1f8fb0123f75e0ce5a46840e558c99c0d8c))
* delete the duplicated old sections ([b37a620](https://gitlab.com/JuananOc/terragrunt-template/commit/b37a620c8e35d376c6ed5e79d28071a4059621f6))
* move the branch configuration to the release configuration ([28960b3](https://gitlab.com/JuananOc/terragrunt-template/commit/28960b30d1ada10dbfc1c6185496649cdae3654b))
* updated release command ([61d0e3d](https://gitlab.com/JuananOc/terragrunt-template/commit/61d0e3db5b93a52a0a2784261c870542599ae9ce))

## [2.0.0](https://gitlab.com/JuananOc/terragrunt-template/compare/v1.0.0...v2.0.0) (2023-09-30)


### ⚠ BREAKING CHANGES

* updated terragrunt template repository structure

### Features

* updated terragrunt template repository structure ([fe8502b](https://gitlab.com/JuananOc/terragrunt-template/commit/fe8502b651c292187ea6101b3534757c7495fe15))

## 1.0.0 (2023-09-30)


### Features

* update makefile with the common actions to handle the repository ([2c1c96c](https://gitlab.com/JuananOc/terragrunt-template/commit/2c1c96c44915c2128c1d41c0ac8146d708a6c5e8))
